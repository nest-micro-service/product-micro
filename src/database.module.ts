import { Module, Global, DynamicModule } from '@nestjs/common'

import { TypeOrmModule } from '@nestjs/typeorm'


import { EnvModule } from './env.module'
import { EnvService } from './env.service';

function DatabaseOrmModule (): DynamicModule {
  const config = new EnvService().read()
  return TypeOrmModule.forRoot({
    type: config.PRODUCT_DB_TYPE,
    host: config.PRODUCT_DB_HOST,
    port: config.PRODUCT_DB_PORT,
    username: config.PRODUCT_DB_USER,
    password: config.PRODUCT_DB_PASSWORD,
    database: config.PRODUCT_DB_NAME,
    entities: ['dist/**/*.entity.{ts,js}'],
    synchronize:config.PRODUCT_DB_SYNCHRONIZE
  })
}

@Global()
@Module({
  imports: [
    EnvModule,
    DatabaseOrmModule()
  ]
})
export class DatabaseModule { }