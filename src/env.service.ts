import * as dotenv from 'dotenv'
import * as fs from 'fs'

export interface EnvData {
  // application
  APP_ENV: string
  APP_DEBUG: boolean
  PRODUCT_HOST:string
  PRODUCT_PORT:number
  // database
  PRODUCT_DB_TYPE: 'mysql' | 'mariadb'
  PRODUCT_DB_HOST?: string
  PRODUCT_DB_NAME: string
  PRODUCT_DB_PORT?: number
  PRODUCT_DB_USER: string
  PRODUCT_DB_PASSWORD: string
  PRODUCT_DB_SYNCHRONIZE: boolean
}

export class EnvService {
  private vars: EnvData

  constructor () {
    const environment = process.env.NODE_ENV || 'development'
   const data: any = dotenv.parse(
      fs.readFileSync(`./../shared-env/${environment}.env`)
    )


    
    data.APP_ENV = environment
    data.APP_DEBUG = data.APP_DEBUG === 'true' ? true : false
    data.PRODUCT_DB_PORT = parseInt(data.PRODUCT_DB_PORT)
    data.PRODUCT_DB_SYNCHRONIZE = data.PRODUCT_DB_SYNCHRONIZE === 'true' ? true : false

    this.vars = data as EnvData
  }

  read (): EnvData {
    return this.vars
  }

  isDev (): boolean {
    return (this.vars.APP_ENV === 'development')
  }

  isProd (): boolean {
    return (this.vars.APP_ENV === 'production')
  }
}