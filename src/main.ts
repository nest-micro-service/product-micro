import { INestMicroservice, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AppModule } from './app.module';
import { protobufPackage } from './product/product.pb';
import { EnvService } from './env.service';

async function bootstrap() {
  const config = new EnvService().read()
  const url = `${config.PRODUCT_HOST}:${config.PRODUCT_PORT}`;
  console.log('url', url);
  const app: INestMicroservice = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.GRPC,
    options: {
      url: url,
      package: protobufPackage,
      protoPath: join('node_modules/grpc-proto/proto/product.proto'),
    },
  });

  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

  await app.listen();

  console.log(`Product Application running at ${url}`)
}

bootstrap();
